
Pod::Spec.new do |s|
  s.name             = 'YPTest'
  s.version          = '0.1.0'
  s.summary          = 'YPTest.'
  s.description      = <<-DESC
YPTestDemo.
                       DESC

  s.homepage         = 'https://git.oschina.net/xuanxiuiOS/YPPodMaker'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '于鹏' => 'yupeng_ios@163.com' }
  s.source           = { :git => 'https://git.oschina.net/xuanxiuiOS/YPPodMaker.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'YPTest/Classes/**/*'
end
